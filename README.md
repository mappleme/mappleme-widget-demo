# mapple.me Widget

## Introduction
This widget displays your community, handles interactions, and communicates with mapple.me rest service.

## Quick Setup
* Create a community on mapple.me
* Place the widget script and html tag in your website's (e.g. https://example.com/community) HTML code, and enter your community shortname in the script tag.

### Add script
```html
<!-- mapple.me sdk start - should be after opening body tag -->
<script>
window.mmAsyncInit = function() {
        mapplemeWidget.init({
                // replace example with the name of your community
                community: 'example',
                prefix: '',
                urltype: 'query',
                pingteaserlimit: 5,
                allpingteaserlimit: 11,
                userteaserlimit: 8,
                clientcallback: "mcallback"
        })
};

(function(d, s, id) {
        var js, mjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://api.mapple.me/api/widget/js/mappleme.js";
        mjs.parentNode.insertBefore(js, mjs);
}(document, 'script', 'mappleme-jssdk')
);</script>
<!-- mapple.me sdk end -->
```

```html
<!-- put this where you want the widget to appear -->
<div id="mappleme-root"></div>
```

### Quick configuration
* Set the domain and path prefix (e.g. https://example.com/community) in your community settings.
* As well as your your Google Maps API key for your domain 
* For facebook link previews to work, add [og headers](https://gitlab.com/mappleme/mappleme-widget-demo/wikis/OpenGraph-meta-header-for-link-sharing-preview) through a server side include.
* For static paths (for SEO and aesthetic reasons) configure a server internal redirect (not a user redirect), and set urltype to 'path'. Example nginx:

```nginx
location ~ ^/community/(.+)$ {
  rewrite ^ /community/ last;
}
```

## More
* [Detailed Setup Guide](https://gitlab.com/mappleme/mappleme-widget-demo/wikis/Setup-premium-community) - detailed setup and configuration.
* [Service Overview Presentation](https://docs.google.com/presentation/d/1nbJSIBP8yKkN9R-BUetKZtDDGkc-k54o7Z9j6PjTnH4/edit#slide=id.g231441b4a8_0_22) - An overview of the mapple.me ecosystem.
* [Wiki](https://gitlab.com/mappleme/mappleme-widget-demo/wikis) - APIs, crawlers and more.

## Demo
* A demo implementation is located under [demo/helloworld.html](https://mapple.me/c/mappleme-widget-demo/demo/helloworld.html).
* Some screenshots:

