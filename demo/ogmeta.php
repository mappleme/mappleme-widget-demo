<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
body {
	margin: 0;
}
</style>
<?php 
// fetch facebook open graph headers for this community content for facebook link preview to work.
// urlType query or path required to work
if (@strpos($_SERVER["HTTP_USER_AGENT"],"facebookexternalhit") !== false)  echo @file_get_contents("https://api.mapple.me/api_alpha/rest/ogmeta?".$_SERVER["REQUEST_URI"]);
?>
</head>
<body>
<!-- mapple.me sdk start - should be after opening body tag -->
<script>

function mmcallback() {

}

window.mmAsyncInit = function() {
        mapplemeWidget.init({
                community: 'selected',
                prefix: '',
                urltype: 'query',
                pingteaserlimit: 5,
                allpingteaserlimit: 11,
                userteaserlimit: 12,
                clientcallback: "mcallback"
            
        })};

(function(d, s, id) {
        var js, mjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "https://api.mapple.me/api/widget/js/mappleme.js";
        mjs.parentNode.insertBefore(js, mjs);
}(document, 'script', 'mappleme-jssdk')
);</script>
<!-- mapple.me sdk end -->

<!-- put this where you want the widget to appear -->
<div id="mappleme-root"></div>

</body>
</html>
